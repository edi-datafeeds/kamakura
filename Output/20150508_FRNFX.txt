EDI_FRNFX_20150508
TableName	Actflag	Created	Changed	FrnfxID	SecID	ISIN	EffectiveDate	OldFRNType	OldFRNIndexBenchmark	OldFrnMargin	OldMinimumInterestRate	OldMaximumInterestRate	OldRounding	NewFRNType	NewFRNindexBenchmark	NewFrnMargin	NewMinimumInterestRate	NewMaximumInterestRate	NewRounding	Eventtype	OldFrnIntAdjFreq	NewFrnIntAdjFreq
FRNFX	U	2011/02/01	2015/05/08	33921	293942	FR0010092908	2015/06/07	COLLAR	EURSWR5Y			5.90000		COLLAR	EURSWR4Y			5.90000		FRNFX		
FRNFX	U	2013/06/14	2015/05/08	48189	2881901	US38143UVG39	2015/06/07	BASIC	USDLIBOR3M	1.35000				COLLAR	USDLIBOR3M	1.35000		7.00000		FRNFX		
FRNFX	I	2015/05/08	2015/05/08	59436	4492345	US3130A53P20	2015/04/22	BASIC	USDLIBOR3M				3	COLLAR	USDLIBOR3M			3.00000	3	CORR		
FRNFX	I	2015/05/08	2015/05/08	59437	4496343	US3130A57H67	2022/05/07	COLLAR	USDLIBOR1M	2.75000		3.00000	3	COLLAR	USDLIBOR1M	2.75000		4.00000	3	FRNFX		
FRNFX	I	2015/05/08	2015/05/08	59439	4496343	US3130A57H67	2024/05/07	COLLAR	USDLIBOR1M	2.75000		3.00000	3	COLLAR	USDLIBOR1M	2.75000		5.00000	3	FRNFX		
FRNFX	I	2015/05/08	2015/05/08	59440	4496343	US3130A57H67	2024/11/07	COLLAR	USDLIBOR1M	2.75000		3.00000	3	COLLAR	USDLIBOR1M	2.75000		6.00000	3	FRNFX		
FRNFX	I	2015/05/08	2015/05/08	59441	4492345	US3130A53P20	2015/04/23	COLLAR	USDLIBOR3M			3.00000	3	COLLAR	USDLIBOR3M	1.00000		3.00000	3	CORR		
EDI_ENDOFFILE
